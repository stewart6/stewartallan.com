FROM node:16.13.0

RUN mkdir -p /local
WORKDIR /local

RUN apt-get update && apt-get -y install curl netcat vim

ENV NPM_CONFIG_LOGLEVEL warn
RUN npm install -g serve

ADD build /local/build
COPY docker/start.sh /local/

RUN chmod +x /local/start.sh

CMD ["/local/start.sh"]