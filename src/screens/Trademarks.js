import React from 'react';
import styles from './Trademarks.module.css'
import {Helmet} from 'react-helmet'
import lex from '../images/Trademarks/lexsearch.png'
import mm from '../images/Trademarks/mrmurdoch.png';
import mrsm from '../images/Trademarks/mrsmurdoch.png'
import gfh from '../images/Trademarks/gfh.png'
import tf from '../images/Trademarks/whitecloset.png'
import swim from '../images/Trademarks/paperheartswim.png'
import urban from '../images/Trademarks/urban.png'
import lol from '../images/Trademarks/labeloflove.png'
import accu from '../images/Trademarks/acculex.png'
import renew from '../images/Trademarks/renew-skin.png'

const Trademarks = () => {

    return (
        <div className={styles.trademarks}>
            <Helmet defaultTitle='trademarks'>
                <meta charSet='utf-8'/>
                <meta name='description' content=''/>
            </Helmet>
            <div className={styles.grid}>
                <div className={styles.gridItem}>
                    <img src={mrsm} alt={''}/>
                </div>
                <div className={styles.gridItem}>
                    <img src={mm} alt={''}/>
                </div>
                <div className={styles.gridItem}>
                    <img src={tf} alt={''}/>
                </div>
                <div className={styles.gridItem}>
                    <img src={lol} alt={''}/>
                </div>
                <div className={styles.gridItem}>
                    <img src={swim} alt={''}/>
                </div>
                <div className={styles.gridItem}>
                    <img src={gfh} alt={''}/>
                </div>
                <div className={styles.gridItem}>
                    <img src={lex} alt={''}/>
                </div>
                <div className={styles.gridItem}>
                    <img src={urban} alt={''}/>
                </div>
                <div className={styles.gridItem}>
                    <img src={accu} alt={''}/>
                </div>
                <div className={styles.gridItem}>
                    <img src={renew} alt={''}/>
                </div>
                <div className={styles.gridItem}>
                    {/*<img src={project52} alt={''}/>*/}
                </div>
            </div>
        </div>
    )
}


export default Trademarks