import React, {useState} from 'react';
import styles from './Home.module.css'
import {Link} from 'react-router-dom'
import Helmet from 'react-helmet'

const Home = () => {
    const [showBackground, setShowBackground] = useState(false);

    const hoverBackground = () => {
        setShowBackground(!showBackground)
    };

    return (
        <div>
            <Helmet>
                <title>home</title>
                <meta name='description' content=''/>
            </Helmet>
            <div className={styles.home}>
                {/*<div className={styles.logoContainer}>
                        <h1>stewart allan</h1>
                    </div>*/}
                <div className={styles.pageContent}>
                    <div className={styles.left}>
                        <div>
                            <Link onMouseEnter={hoverBackground} onMouseLeave={hoverBackground}
                                  to='/projects'>
                                projects
                                {/*<img src={proj}
                                     className={styles.projectBG + (this.state.showBackground ? ' ' + styles.bgshow : ' ' + styles.bghide)}
                                     alt={''}/>*/}</Link>
                        </div>
                        <div>
                            <Link to={'/trademarks'}>trademarks</Link>
                        </div>
                        <div>
                            <Link to={'/contact'}>contact</Link>
                        </div>
                        <div>
                            <Link to={'/about'}>about</Link>
                        </div>
                    </div>
                    <div className={styles.right}>
                            <span>
                                aspiring to never become complacent
                            </span>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Home