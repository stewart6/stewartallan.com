import React from 'react';
import styles from './Landing.module.css'
import {Link} from 'react-router-dom'
import curiosity from '../images/curiosity.gif'

const Landing = () => {

    return (
        <div className={styles.landing}>
            <Link to='/meet-me' className={styles.image}>
                <img src={curiosity} alt={''}/>
            </Link>
        </div>
    )
}

export default Landing